# Hexo Bulma Test Drive

An example of Hexo site using Bulma for personal learning purpose.

This is basically a Bulma version of Hugo Tutorial.

![Hexo Bulma: Preview][hexo-bulma-preview]

Heelo world
-- -- --

## Links

### Demo Site

* <https://akutidaktahu.netlify.com/>

### Presentation

* [Hexo - Presentation Slide][hexo-presentation]

### Hexo Article Series

Consist of more than 30 articles.

* [Hexo - Overview][hexo-overview]

* [Hexo - Summary][hexo-summary]

* [Hexo Step by Step Repository][tutorial-hexo] (this repository)

### Bulma Article Series

Consist of 10 articles.

* [Bulma - Overview][bulma-overview]

* [Bulma Step by Step Repository][tutorial-bulma]

[hexo-presentation]: https://epsi-rns.gitlab.io/ssg/2019/05/30/hexo-presentation/
[hexo-overview]:     https://epsi-rns.gitlab.io/ssg/2019/05/01/hexo-overview/
[hexo-summary]:      https://epsi-rns.gitlab.io/ssg/2019/05/30/hexo-summary/
[bulma-overview]:    https://epsi-rns.gitlab.io/frontend/2019/03/01/bulma-overview/

[tutorial-pelican]:     https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/
[tutorial-hexo]:     https://gitlab.com/epsi-rns/tutor-hexo-bulma/
[tutorial-bulma]:    https://gitlab.com/epsi-rns/tutor-html-bulma/


### Comparation

Comparation with other static site generator

* [Jekyll Step by Step Repository][tutorial-jekyll]

* [Hugo Step by Step Repository][tutorial-hugo]

* [Eleventy Step by Step Repository][tutorial-11ty]

* [Pelican Step by Step Repository][tutorial-pelican]

### Presentation

* [Concept SSG - Presentation Slide][ssg-presentation]

* [Concept CSS - Presentation Slide][css-presentation]

[tutorial-pelican]:     https://gitlab.com/epsi-rns/tutor-pelican-bulma-md/
[tutorial-jekyll]:      https://gitlab.com/epsi-rns/tutor-jekyll-bulma-md/
[tutorial-hugo]:        https://gitlab.com/epsi-rns/tutor-hugo-bulma/
[tutorial-11ty]:        https://gitlab.com/epsi-rns/tutor-11ty-materialize/
[tutorial-hexo]:        https://gitlab.com/epsi-rns/tutor-hexo-bulma/

[ssg-presentation]:     https://epsi-rns.gitlab.io/ssg/2019/02/17/concept-ssg/
[css-presentation]:     https://epsi-rns.gitlab.io/frontend/2019/02/15/concept-css/

-- -- --

## Chapter Step by Step

### Tutor 01

> Generate Only Pure HTML

* Installing Hexo

* Setup Directory for Minimal Hexo

* General Layout: Base, Page, Post, Index

* Basic Content

![Hexo Bulma: Tutor 01][tutor-01]

### Tutor 02

> Adding stylesheet for content

* Add Bulma CSS

* Standard Header and Footer

* Enhance All Layouts with Bulma CSS

![Hexo Bulma: Tutor 02][tutor-02]

### Tutor 03

* Add Custom SASS (Custom Design)

* Nice Header and Footer

* Custom Pages: Categories, Tags, Home (landing page)

* Apply Two Column Responsive Layout for Most Layout

* Additional Layout: Category, Tag, Archive

* Template Refactor: Reusable Post List

* EJS: Header Title

* Languages

![Hexo Bulma: Tutor 03][tutor-03]

### Tutor 04

* More Content: Lyrics and Quotes. Need this content for demo

* Template: Blog Item

* Template: Post List: Simple, By Year, List Tree (By Year and Month)

* Tags and Categories Page: Nice Tag Badge and List Tree

* Multi Column Responsive List: Tags, Categories, and Archive

* Widget: Friends, Archives Tree, Categories, Tags, Recent Post, Related Post

* Custom Output: JSON

![Hexo Bulma: Tutor 04][tutor-04]

### Tutor 05

> Optional Feature

* Blog Pagination: Adjacent, Indicator, Responsive.

* More Pagination: Tags, Categories, and Archive

![Hexo Bulma: Tutor 05: Indicator Pagination][tutor-05indi]

![Hexo Bulma: Tutor 05: Responsive Pagination][tutor-05resp]

> Finishing

* Post: Header, Footer, Navigation

* Post: Markdown Content

* Post: Table of Content

* Post: Responsive Images

* Syntax Highlight: CSS Fix

* Meta: HTML, SEO, Opengraph, Twitter

![Hexo Bulma: Tutor 06][tutor-06]

-- -- --

What do you think ?
  
[hexo-bulma-preview]:   https://gitlab.com/epsi-rns/tutor-hexo-bulma/raw/master/preview/hexo-bulma-preview.png
[tutor-01]:     https://gitlab.com/epsi-rns/tutor-hexo-bulma/raw/master/preview/13-browser-landing-page.png
[tutor-02]:     https://gitlab.com/epsi-rns/tutor-hexo-bulma/raw/master/preview/25-browser-post.png
[tutor-03]:     https://gitlab.com/epsi-rns/tutor-hexo-bulma/raw/master/preview/38-browser-cats-tree.png
[tutor-04]:     https://gitlab.com/epsi-rns/tutor-hexo-bulma/raw/master/preview/42-section-category.png
[tutor-05indi]: https://gitlab.com/epsi-rns/tutor-hexo-bulma/raw/master/preview/54-hexo-bulma-indicator-animate.gif
[tutor-05resp]: https://gitlab.com/epsi-rns/tutor-hexo-bulma/raw/master/preview/55-hexo-bulma-responsive-animate.gif
[tutor-06]:     https://gitlab.com/epsi-rns/tutor-hexo-bulma/raw/master/preview/61-blog-post-all.png
